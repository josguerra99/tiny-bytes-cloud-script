using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Company.Function
{
    public static class AddCurrency
    {
        [FunctionName("AddCurrency")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            try
            {
                var (playfabId, args, context) = await Helper.GetFunctionArguments(req);
                var amount = (int)args.amount;
                var currencyId = (string)args.currencyId;

                if (currencyId == Helper.SoftCoinsId)
                    await TestingController.GrandSoftCoins(playfabId, amount);
                else if (currencyId == Helper.HardCoinsId)
                    await TestingController.GrantHardCoins(playfabId, amount);

                return Response.Ok("Currency added successfully");
            }
            catch (Exception e)
            {
                return Response.Error(e.Message);
            }
        }
    }
}
