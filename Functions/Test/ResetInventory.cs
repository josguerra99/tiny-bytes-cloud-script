using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Company.Function
{
    public static class ResetInventory
    {
        [FunctionName("ResetInventory")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            try
            {
                var (playfabId, args, context) = await Helper.GetFunctionArguments(req);
                await TestingController.ResetInventory(playfabId);
                return Response.Ok("Inventory was reset successfully");
            }
            catch (Exception e)
            {
                return Response.Error(e.Message);
            }
        }
    }
}
