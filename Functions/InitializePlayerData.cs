using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Company.Function
{
    public static class InitializePlayerData
    {
        [FunctionName("InitializePlayerData")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {

            try
            {
                var (playfabId, args, context) = await Helper.GetFunctionArguments(req);
                await InventoryController.GrantPlayerInitialItems(playfabId);
                return Response.Ok("Player data initialized successfully");

            }
            catch (Exception e)
            {
                return Response.Error(e.Message);
            }
        }
    }
}
