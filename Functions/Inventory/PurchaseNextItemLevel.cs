using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Company.Function
{
    public static class PurchaseNextItemLevel
    {
        [FunctionName("PurchaseNextItemLevel")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {

            try
            {
                var (playfabId, args, context) = await Helper.GetFunctionArguments(req);
                var itemId = (string)args.itemId;

                var result = await InventoryController.PurchaseNextLevel(playfabId, itemId);
                return Response.Ok(JsonConvert.SerializeObject(result));
            }
            catch (Exception e)
            {
                return Response.Error(e.Message);
            }


        }
    }
}
