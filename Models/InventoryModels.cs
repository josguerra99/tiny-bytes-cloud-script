using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Globalization;


/// <summary>
/// This is a model for a player's item
/// It contains an Inventory Item and a Catalog Item
/// And it also contains helper properties, that are used to determine if the item can be unlocked,
/// the current level of the item, and the next level of the item, etc.
/// </summary>
public struct LevelableInventoryItem
{
    public string ItemId { get; private set; }
    public CatalogItem CatalogItem { get; private set; }
    public InventoryItem InventoryItem { get; private set; }
    public int SoftCoins { get; private set; }
    public int HardCoins { get; private set; }

    public LevelableInventoryItem(PlayFab.ServerModels.CatalogItem catalogItem, PlayFab.ServerModels.ItemInstance inventoryItem, int softCoins, int hardCoins)
    {
        ItemId = catalogItem.ItemId;
        CatalogItem = new CatalogItem(catalogItem);

        if (inventoryItem != null)
            InventoryItem = new InventoryItem(inventoryItem);
        else
            InventoryItem = new InventoryItem(ItemId);

        SoftCoins = softCoins;
        HardCoins = hardCoins;

    }

    public int CurrentItemLevel => InventoryItem.customData.currentLevel;
    public int CurrentUsableItemLevel => InventoryItem.customData.currentUsableLevel;
    public int MaxLevel => CatalogItem.customData.levels.Count - 1;
    public bool HasReachedMaxLevel => CurrentItemLevel == MaxLevel;
    public int LevelToPurchase => HasReachedMaxLevel ? -1 : CurrentItemLevel + 1;
    public bool HasUpgradePending => CurrentItemLevel != -1 && CurrentItemLevel != CurrentUsableItemLevel;
    public bool CanPurchaseNextLevel => (!HasUpgradePending && !HasReachedMaxLevel && SoftCoins >= CatalogItem.customData.levels[LevelToPurchase].sc);
    public bool CanSkipLevel => HasUpgradePending && HardCoins >= CatalogItem.customData.levels[CurrentItemLevel].hc;
    public int CostOfNextUpgrade => HasReachedMaxLevel ? 0 : CatalogItem.customData.levels[LevelToPurchase].sc;
    public int CostOfSkip => CurrentItemLevel == -1 ? 0 : CatalogItem.customData.levels[CurrentItemLevel].hc;
    public double TimeSinceLastUnlock => (DateTime.UtcNow - InventoryItem.customData.lastUnlockTime).TotalSeconds;
    public double RemainingTimeToUnlock => CatalogItem.customData.levels[CurrentItemLevel].time - TimeSinceLastUnlock;
}


/// <summary>
/// This will conver the Playfab's inventory item into this struct
/// that way it will be easier to access every variable inside
/// the custom data field
///</summary>
public struct InventoryItem
{
    public string itemId;
    public string itemInstanceId;
    public InventoryItemCustomData customData;

    public InventoryItem(PlayFab.ServerModels.ItemInstance playfabInventoryItem)
    {
        this.itemId = playfabInventoryItem.ItemId;
        this.itemInstanceId = playfabInventoryItem.ItemInstanceId;
        this.customData = new InventoryItemCustomData(playfabInventoryItem.CustomData);
    }

    public InventoryItem(string itemId)
    {
        this.itemId = itemId;
        itemInstanceId = null;
        customData = default;
        customData.currentLevel = -1;
        customData.currentUsableLevel = -1;
    }
}

/// <summary>
/// This struct will conver the playfab's item custom data string
/// into a struct
/// </summary>

[Serializable]
public struct InventoryItemCustomData
{
    public int currentUsableLevel;
    public int currentLevel;
    public DateTime lastUnlockTime;

    public InventoryItemCustomData(Dictionary<string, string> playfabCustomData)
    {
        this.currentUsableLevel = int.Parse(playfabCustomData["currentUsableLevel"]);
        this.currentLevel = int.Parse(playfabCustomData["currentLevel"]);
        this.lastUnlockTime = DateTime.ParseExact(playfabCustomData["lastUnlockTime"], "yyyy-MM-dd HH:mm:ss,fff", CultureInfo.InvariantCulture);
    }
}



/// <summary>
/// This will convert the Playfab's catalog item into this struct,
/// that way it will be easier to access every variable inside
/// the custom data field
/// </summary>
public struct CatalogItem
{
    public string itemId;
    public CatalogItemCustomData customData;
    public string itemClass;

    public CatalogItem(PlayFab.ServerModels.CatalogItem playfabCatalogItem)
    {
        this.itemClass = playfabCatalogItem.ItemClass;
        this.itemId = playfabCatalogItem.ItemId;
        this.customData = JsonConvert.DeserializeObject<CatalogItemCustomData>(playfabCatalogItem.CustomData);
    }
}

/// <summary>
/// This is struct will convert the playtfab's CatalogItem custom data string
/// into a struct
///</summary>
[Serializable]
public struct CatalogItemCustomData
{
    public List<CatalogItemLevelData> levels;
}

/// <summary>
/// Data for a specific item's level
/// </summary>
[Serializable]
public struct CatalogItemLevelData
{
    public int sc;
    public int hc;
    public int time;
}

