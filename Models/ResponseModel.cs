using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

[System.Serializable]
public struct ResponseModel
{
    public string Message { get; set; }
    public string Error { get; set; }

}


/// <summary>
/// Helper class that creates a OkObjectResult with a JSON string from a ResponseModel
/// </summary>
public class Response
{

    public static OkObjectResult Ok(string message = "")
    {
        return new OkObjectResult(
            JsonConvert.SerializeObject(new ResponseModel() { Message = message, Error = null })
        );
    }

    public static OkObjectResult Error(string error = "")
    {
        return new OkObjectResult(
            JsonConvert.SerializeObject(new ResponseModel() { Message = null, Error = error })
        );
    }
}



