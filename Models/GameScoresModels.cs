using System.Collections.Generic;


/// <summary>
/// Represents the data that will be send as a response after calling
/// the FinishGameController.FinishGame method
///</summary>

[System.Serializable]
public struct FinishGameModelResult
{
    public int softCoins;
    public int score;
    public float time;

}




/// <summary>
/// Represents a leaderboard entry 
/// </summary>
public struct LeaderboardItemByScore
{
    public string playfabId;
    public string displayName;
    public int score;
}


public struct LeaderboardItemByTime
{
    public string playfabId;
    public string displayName;
    public float time;
}