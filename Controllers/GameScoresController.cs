using System.Threading.Tasks;
using System.Collections.Generic;

public static class GameScoresController
{


    /// <summary>
    /// After a game finish this method must be called, it will add Soft Coins to the players,
    /// and will add the score to the leaderboards
    /// </summary>
    public static async Task<FinishGameModelResult> FinishGame(string playfabId, int score, float time)
    {

        var finishGameModel = new FinishGameModelResult()
        {
            score = score,
            time = time,
            softCoins = score * 10
        };

        //Increase currency
        await Helper.ServerApi.AddUserVirtualCurrencyAsync(new PlayFab.ServerModels.AddUserVirtualCurrencyRequest()
        {
            Amount = finishGameModel.softCoins,
            PlayFabId = playfabId,
            VirtualCurrency = Helper.SoftCoinsId
        });



        //Add score to leaderboard
        await Helper.ServerApi.UpdatePlayerStatisticsAsync(new PlayFab.ServerModels.UpdatePlayerStatisticsRequest()
        {
            PlayFabId = playfabId,
            Statistics = new List<PlayFab.ServerModels.StatisticUpdate>()
            {
                new PlayFab.ServerModels.StatisticUpdate()
                {
                    StatisticName = Helper.ScoreLeaderboardId,
                    Value = score
                }
            }
        });




        return finishGameModel;
    }

}