using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Globalization;



public class InventoryController
{

    /*
supernova
atomic annie
moonshight
epilogue
endbringer
*/



    /// <summary>
    /// Sets the item as the current selected item
    /// </summary>
    public static async Task SelectItem(string playfabId, string category, string itemId)
    {

        var levelableItem = await GetLevelableInventoryItemFromPlayer(playfabId, itemId);

        if (levelableItem.CurrentUsableItemLevel == -1)
            throw new Exception("Item is not unlocked yet");


        if (category != levelableItem.CatalogItem.itemClass)
            throw new Exception("Item is not of the same category");


        await Helper.ServerApi.UpdateUserReadOnlyDataAsync(new PlayFab.ServerModels.UpdateUserDataRequest()
        {
            PlayFabId = playfabId,
            Data = new Dictionary<string, string>()
            {
                { $"Current.{category}" , itemId }
            }
        });

    }




    /// <summary>
    /// When a new player signs up, this function will be grant the player the default spaceship and turret
    /// </summary>
    public static async Task GrantPlayerInitialItems(string playfabId)
    {


        var inventory = await Helper.ServerApi.GetUserInventoryAsync(new PlayFab.ServerModels.GetUserInventoryRequest()
        {
            PlayFabId = playfabId
        });


        PlayFab.ServerModels.ItemInstance spaceship = null;
        PlayFab.ServerModels.ItemInstance turret = null;

        foreach (var item in inventory.Result.Inventory)
        {
            if (item.ItemId == Helper.DefaultSpaceshipId)
            {
                spaceship = item;
            }

            if (item.ItemId == Helper.DefaultTurretId)
            {
                turret = item;
            }

            if (spaceship != null && turret != null)
            {
                return;
            }
        }


        var customData = new Dictionary<string, string>(){
                {"currentUsableLevel", "0"},
                {"currentLevel", "0"},
                {"lastUnlockTime", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss,fff",CultureInfo.InvariantCulture)}
                };


        List<Task> tasks = new List<Task>();

        if (spaceship == null)
            tasks.Add(GrantFreeItem(playfabId, Helper.DefaultSpaceshipId, customData));

        if (turret == null)
            tasks.Add(GrantFreeItem(playfabId, Helper.DefaultTurretId, customData));

        await Task.WhenAll(tasks.ToArray());


        tasks.Clear();

        //Select the newly granted items

        if (spaceship == null)
            tasks.Add(SelectItem(playfabId, Helper.SpaceshipCategoryId, Helper.DefaultSpaceshipId));

        if (turret == null)
            tasks.Add(SelectItem(playfabId, Helper.TurretCategoryId, Helper.DefaultTurretId));


        await Task.WhenAll(tasks.ToArray());

    }



    /// <summary>
    /// Grants the player an item, without substracting currency
    /// </summary>
    public static async Task GrantFreeItem(string playfabId, string itemId, Dictionary<string, string> customData = null)
    {

        var item = await Helper.ServerApi.GrantItemsToUserAsync(new PlayFab.ServerModels.GrantItemsToUserRequest()
        {
            PlayFabId = playfabId,
            ItemIds = new List<string>() { itemId },
            CatalogVersion = Helper.CatalogId,
            Annotation = "Free Item"
        });



        if (customData != null)
        {
            var itemInstanceId = item.Result.ItemGrantResults[0].ItemInstanceId;
            await Helper.ServerApi.UpdateUserInventoryItemCustomDataAsync(new PlayFab.ServerModels.UpdateUserInventoryItemDataRequest()
            {
                PlayFabId = playfabId,
                ItemInstanceId = itemInstanceId,
                Data = customData
            });
        }

    }


    /// <summary>
    /// This will unlock the next level of the item
    /// </summary>
    public static async Task<InventoryItem> PurchaseNextLevel(string playfabId, string itemId)
    {


        var levelableItem = await GetLevelableInventoryItemFromPlayer(playfabId, itemId);

        //Check if the item can be unlocked
        if (!levelableItem.CanPurchaseNextLevel)
            throw new Exception("Item cannot be unlocked");


        var inventoryItemInstanceId = levelableItem.InventoryItem.itemInstanceId;

        //If the item hasn't been added to the player's inventory yet, add it
        if (levelableItem.CurrentItemLevel == -1)
        {
            var inventoryItemResult = await Helper.ServerApi.GrantItemsToUserAsync(new PlayFab.ServerModels.GrantItemsToUserRequest()
            {
                ItemIds = new List<string>() { itemId },
                PlayFabId = playfabId,
                CatalogVersion = Helper.CatalogId
            });

            inventoryItemInstanceId = inventoryItemResult.Result.ItemGrantResults[0].ItemInstanceId;
        }


        var customDataAfterPurchase = new Dictionary<string, string>(){
                {"currentLevel", levelableItem.LevelToPurchase.ToString()},
                {"currentUsableLevel", levelableItem.CurrentUsableItemLevel.ToString()},
                {"lastUnlockTime", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss,fff",CultureInfo.InvariantCulture)}
            };



        var timeToUnlock = levelableItem.CatalogItem.customData.levels[levelableItem.LevelToPurchase].time;

        if (timeToUnlock == 0)
        {
            customDataAfterPurchase["currentUsableLevel"] = levelableItem.LevelToPurchase.ToString();
        }


        //Then we will update the inventory item's custom data
        await Helper.ServerApi.UpdateUserInventoryItemCustomDataAsync(new PlayFab.ServerModels.UpdateUserInventoryItemDataRequest()
        {

            PlayFabId = playfabId,
            ItemInstanceId = inventoryItemInstanceId,
            Data = customDataAfterPurchase
        });



        //Then we will update the player's soft coins
        var result = await Helper.ServerApi.SubtractUserVirtualCurrencyAsync(new PlayFab.ServerModels.SubtractUserVirtualCurrencyRequest()
        {
            PlayFabId = playfabId,
            VirtualCurrency = Helper.SoftCoinsId,
            Amount = levelableItem.CatalogItem.customData.levels[levelableItem.LevelToPurchase].sc
        });




        return new InventoryItem()
        {
            itemId = levelableItem.InventoryItem.itemId,
            itemInstanceId = inventoryItemInstanceId,
            customData = new InventoryItemCustomData(customDataAfterPurchase)
        };


    }



    /// <summary>
    /// Skips the wait time of the item
    /// </summary>
    public static async Task<InventoryItem> SkipItemWaitTime(string playfabId, string itemId, int levelToSkip)
    {

        var levelableItem = await GetLevelableInventoryItemFromPlayer(playfabId, itemId);

        //Check if the item can be unlocked
        if (!levelableItem.CanSkipLevel || levelableItem.CurrentItemLevel != levelToSkip)
            throw new Exception("Item wait time cannot be skipped");

        var customDataAfterSkip = new Dictionary<string, string>(){
                {"currentLevel", levelableItem.CurrentItemLevel.ToString()},
                {"currentUsableLevel", levelableItem.CurrentItemLevel.ToString()},
                {"lastUnlockTime", levelableItem.InventoryItem.customData.lastUnlockTime.ToString("yyyy-MM-dd HH:mm:ss,fff",CultureInfo.InvariantCulture)}
            };


        //Then we will update the inventory item's custom data
        await Helper.ServerApi.UpdateUserInventoryItemCustomDataAsync(new PlayFab.ServerModels.UpdateUserInventoryItemDataRequest()
        {

            PlayFabId = playfabId,
            ItemInstanceId = levelableItem.InventoryItem.itemInstanceId,
            Data = customDataAfterSkip
        });


        //And substract the hard coins
        await Helper.ServerApi.SubtractUserVirtualCurrencyAsync(new PlayFab.ServerModels.SubtractUserVirtualCurrencyRequest()
        {
            PlayFabId = playfabId,
            VirtualCurrency = Helper.HardCoinsId,
            Amount = levelableItem.CatalogItem.customData.levels[levelableItem.CurrentItemLevel].hc
        });


        return new InventoryItem()
        {
            itemId = levelableItem.InventoryItem.itemId,
            itemInstanceId = levelableItem.InventoryItem.itemInstanceId,
            customData = new InventoryItemCustomData(customDataAfterSkip)
        };
    }


    /// <summary>
    /// Checks if the unlock time of the item has passed, if it has passed, it will update the item's custom data
    /// </summary>
    public static async Task<InventoryItem> UnlockItem(string playfabId, string itemId, int levelToTest)
    {


        var levelableItem = await GetLevelableInventoryItemFromPlayer(playfabId, itemId);

        if (!levelableItem.HasUpgradePending || levelableItem.CurrentItemLevel != levelToTest)
            throw new Exception("Item cannot be unlocked");

        var currentTime = DateTime.UtcNow;
        var lastUnlockTime = levelableItem.InventoryItem.customData.lastUnlockTime;
        var secondsToWait = levelableItem.CatalogItem.customData.levels[levelableItem.CurrentItemLevel].time;

        //Check if the item can be unlocked
        if ((currentTime - lastUnlockTime).TotalSeconds < secondsToWait)
            throw new Exception("Item cannot be unlocked");


        var customDataAfterUnlock = new Dictionary<string, string>(){
                {"currentLevel", levelableItem.CurrentItemLevel.ToString()},
                {"currentUsableLevel", levelableItem.CurrentItemLevel.ToString()},
                {"lastUnlockTime", levelableItem.InventoryItem.customData.lastUnlockTime.ToString("yyyy-MM-dd HH:mm:ss,fff",CultureInfo.InvariantCulture)}
            };



        //Then we will update the inventory item's custom data
        await Helper.ServerApi.UpdateUserInventoryItemCustomDataAsync(new PlayFab.ServerModels.UpdateUserInventoryItemDataRequest()
        {

            PlayFabId = playfabId,
            ItemInstanceId = levelableItem.InventoryItem.itemInstanceId,
            Data = customDataAfterUnlock
        });




        return new InventoryItem()
        {
            itemId = levelableItem.InventoryItem.itemId,
            itemInstanceId = levelableItem.InventoryItem.itemInstanceId,
            customData = new InventoryItemCustomData(customDataAfterUnlock)
        };

    }



    /// <summary>
    /// Gets the item from the catalog and from the player's inventory and then creates a model (LevelableInventoryItem)
    /// <param name="playfabId">The player's playfab id</param>
    /// <param name="itemId">The item's id</param>
    /// </summary>
    private static async Task<LevelableInventoryItem> GetLevelableInventoryItemFromPlayer(string playfabId, string itemId)
    {

        //Gets the catalog's item by id
        var catalog = await Helper.ServerApi.GetCatalogItemsAsync(new PlayFab.ServerModels.GetCatalogItemsRequest()
        {
            CatalogVersion = Helper.CatalogId
        });

        var catalogItem = catalog.Result.Catalog.Find(x => x.ItemId == itemId);

        if (catalogItem == null)
            throw new Exception("Item not found");



        //Gets the inventory item by id
        var inventory = await Helper.ServerApi.GetUserInventoryAsync(new PlayFab.ServerModels.GetUserInventoryRequest()
        {
            PlayFabId = playfabId
        });

        var inventoryItem = inventory.Result.Inventory.Find(x => x.ItemId == itemId);



        //Gets the player's soft coins
        var softCoins = inventory.Result.VirtualCurrency[Helper.SoftCoinsId];

        //Gets the player's hard coins
        var hardCoins = inventory.Result.VirtualCurrency[Helper.HardCoinsId];



        //Converts the data to a levelable item, so this way we will have some helper functions and properties
        var levelableItem = new LevelableInventoryItem(catalogItem, inventoryItem, softCoins, hardCoins);

        return levelableItem;
    }
}
