using System;
using System.Collections.Generic;
using System.Threading.Tasks;




///<summary>
/// THIS CLASS SHOULD NOT BE USED IN THE FINAL PRODUCT
///</summary>
public class TestingController
{

    public static async Task ResetInventory(string playfabId)
    {
        var inventory = await Helper.ServerApi.GetUserInventoryAsync(new PlayFab.ServerModels.GetUserInventoryRequest()
        {
            PlayFabId = playfabId
        });


        List<string> itemsToRemove = new List<string>();


        foreach (var item in inventory.Result.Inventory)
        {
            itemsToRemove.Add(item.ItemInstanceId);
        }



        //Doing them in sequence (so that way we don't get blocked for making to many api calls per second)

        foreach (var item in itemsToRemove)
        {

            await Helper.ServerApi.RevokeInventoryItemAsync(new PlayFab.ServerModels.RevokeInventoryItemRequest()
            {
                PlayFabId = playfabId,
                ItemInstanceId = item
            });

        }

    }


    public static async Task GrandSoftCoins(string playfabId, int amount)
    {
        await Helper.ServerApi.AddUserVirtualCurrencyAsync(new PlayFab.ServerModels.AddUserVirtualCurrencyRequest()
        {
            Amount = amount,
            PlayFabId = playfabId,
            VirtualCurrency = Helper.SoftCoinsId
        });
    }

    public static async Task GrantHardCoins(string playfabId, int amount)
    {

        await Helper.ServerApi.AddUserVirtualCurrencyAsync(new PlayFab.ServerModels.AddUserVirtualCurrencyRequest()
        {
            Amount = amount,
            PlayFabId = playfabId,
            VirtualCurrency = Helper.HardCoinsId
        });

    }
}

