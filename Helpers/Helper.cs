using System;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using PlayFab.Samples;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Mvc;


public static class Helper
{
    public static readonly string ScoreLeaderboardId = "High Score";
    public static readonly string TimeLeaderboardId = "Time";
    public static readonly string CatalogId = "My Catalog";
    public static readonly string SoftCoinsId = "SC";
    public static readonly string HardCoinsId = "HC";
    public static readonly string DefaultSpaceshipId = "micro_recon";
    public static readonly string DefaultTurretId = "echo";

    public static readonly string TurretCategoryId = "Turret";
    public static readonly string SpaceshipCategoryId = "Spaceship";

    /// <summary>
    /// This is a helper funciton that helps with boilerplate code
    /// </summary>
    public static async Task<(string, dynamic, FunctionExecutionContext<dynamic>)> GetFunctionArguments(HttpRequest req)
    {

        var requestString = await req.ReadAsStringAsync();
        var context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(requestString);
        var args = context.FunctionArgument;
        var playfabId = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
        return (playfabId, args, context);
    }



    /// <summary>
    /// This is a helper class to help with the boilerplate code required to execute a CloudScript function.    
    /// </summary>
    public static PlayFab.PlayFabServerInstanceAPI ServerApi
    {

        get

        {
            var serverApi = new PlayFab.PlayFabServerInstanceAPI(_settings);
            return serverApi;
        }

    }


    private static PlayFab.PlayFabApiSettings _settings
    {
        get
        {
            var settings = new PlayFab.PlayFabApiSettings();
            settings.DeveloperSecretKey = Environment.GetEnvironmentVariable("PLAYFAB_DEV_SECRET_KEY", EnvironmentVariableTarget.Process);
            settings.TitleId = Environment.GetEnvironmentVariable("PLAYFAB_TITLE_ID", EnvironmentVariableTarget.Process);
            return settings;
        }
    }



}